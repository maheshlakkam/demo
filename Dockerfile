FROM openjdk:11
ARG JAR_FILE=/opt/atlassian/pipelines/agent/build/target/demo-1.0.1.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]